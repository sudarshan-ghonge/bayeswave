#!/usr/bin/env python
import subprocess, os, sys
from bayeswave_plot import BW_Flags as bwf

def delete_lines(filename, file_length):
    # delete all lines except the first `file_length` lines

    # get number of lines currently in the file 
    try:
        N_lines = int(subprocess.check_output("wc -l <{filename}".format(filename = filename), shell=True))
    except subprocess.CalledProcessError:
        # returns if file does not exist
        return
    if N_lines == file_length:
        return
    elif N_lines < file_length:
        print("ERROR!! File {filename} too short! Should be {file_length} but is {N_lines}".format(filename = filename, file_length = file_length, N_lines = N_lines))
    else:
        print("Deleting {0} lines from {filename} ".format(N_lines - file_length, filename = filename))
    # deletes from file_length (exclusive) to end of file
    subprocess.run("sed -i '{file_length},$ d' {filename}".format(filename = filename, file_length = file_length+1), shell = True)

# Read in trigger directory name 
trigdir =  str(sys.argv[1])
runFlags = bwf.Flags(trigdir)
try:
    bwf.readbwb(runFlags)
except FileNotFoundError:
    sys.exit("WARNING: " + runFlags.trigdir + '/bayeswave.run' + " has yet to be created, returning")

if not runFlags.checkpoint:
    sys.exit("--checkpoint has not been turned on, returning")

# read in the model flag to see what checkpoint iteration we should be at
f = open(runFlags.trigdir + '/checkpoint/temperature.dat')
file_length = int(f.readlines()[0]) // runFlags.Ncycle 
f.close()

# get name of model
try:
    f = open(trigdir + '/checkpoint/state.dat', 'r')
except:
    sys.exit(runFlags.trigdir + '/checkpoint/state.dat' + " has yet to be created, returning")
model = f.readlines()[0].split()[0] # first entry is what model we are on
f.close()
print("Model is {model}".format(model = model))

# Get files relating to model 
chaindir = runFlags.trigdir + '/chains'
onlyfiles = [f for f in os.listdir(chaindir) if os.path.isfile(os.path.join(chaindir, f))]
model_files = [f for f in onlyfiles if (model in f)]

# if model is cbc, cbc_skychain.dat should not be considered part of the cbc files
if model == 'cbc':
    model_files.remove('cbc_skychain.dat')

# delete extra lines from files 
for f in model_files:
     delete_lines(runFlags.trigdir + '/chains/' + f, file_length)
